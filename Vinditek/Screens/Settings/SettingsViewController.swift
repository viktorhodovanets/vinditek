//
//  SettingsViewController.swift
//  Vinditek
//
//  Created by Viktor Hodovanets on 25/03/2019.
//  Copyright © 2019 Viktor Hodovanets. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {

    // MARK: - Outlets

    @IBOutlet private weak var cacheCostLimitSlider: UISlider!
    @IBOutlet private weak var cacheCountLimitSlider: UISlider!

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        setupSliders()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if isMovingFromParent {
            updateCache()
        }
    }

    // MARK: - Private

    private func setupSliders() {
        let cacheCountLimit = UserDefaults.standard.object(forKey: Constants.UserDefaults.cacheCountLimit) as? Float ?? 0
        let cacheCostLimit = UserDefaults.standard.object(forKey: Constants.UserDefaults.cacheCostLimit) as? Float ?? 0
        cacheCountLimitSlider.setValue(cacheCountLimit, animated: false)
        cacheCostLimitSlider.setValue(cacheCostLimit, animated: false)
    }

    private func saveCacheSettings() {
        UserDefaults.standard.setValue(Int(cacheCountLimitSlider.value), forKey: Constants.UserDefaults.cacheCountLimit)
        UserDefaults.standard.setValue(Int(cacheCostLimitSlider.value), forKey: Constants.UserDefaults.cacheCostLimit)
    }

    private func setupCache() {
        CacheManager.shared.changeCacheCostLimit(in: Int(cacheCostLimitSlider.value))
        CacheManager.shared.changeCacheCountLimit(count: Int(cacheCountLimitSlider.value))
    }

    private func updateCache() {
        CacheManager.shared.clearCache()
        saveCacheSettings()
        setupCache()
    }

    // MARK: - Public

}
