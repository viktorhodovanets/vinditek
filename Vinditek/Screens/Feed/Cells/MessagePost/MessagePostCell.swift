//
//  MessagePostCell.swift
//  Vinditek
//
//  Created by Viktor Hodovanets on 25/03/2019.
//  Copyright © 2019 Viktor Hodovanets. All rights reserved.
//

import UIKit

class MessagePostCell: UITableViewCell {

    // MARK: - Outlets

    @IBOutlet private weak var postImage: UIImageView!
    @IBOutlet private weak var userName: UILabel!
    @IBOutlet private weak var date: UILabel!
    @IBOutlet private weak var messageView: UITextView!
    @IBOutlet private weak var commentsCount: UILabel!

    // MARK: - Lifecycle

    override func awakeFromNib() {
        postImage.setRounded()
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        postImage.image = nil
    }

    // MARK: - Private

    // MARK: - Public

    func setup(_ model: Post) {
        userName.text = model.userName
        commentsCount.text = String(model.commentsCount)
        messageView.text = model.message
        date.text = model.dateCreated.toString(with: DateFormatter.PostDateFormat)
        postImage.loadImage(from: model.imageURL)
    }
}
