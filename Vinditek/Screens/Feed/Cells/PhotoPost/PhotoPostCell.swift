//
//  PhotoPostCell.swift
//  Vinditek
//
//  Created by Viktor Hodovanets on 25/03/2019.
//  Copyright © 2019 Viktor Hodovanets. All rights reserved.
//

import UIKit

class PhotoPostCell: UITableViewCell {

    // MARK: - Outlets

    @IBOutlet private weak var userName: UILabel!
    @IBOutlet private weak var postImage: UIImageView!
    @IBOutlet private weak var commentLabel: UILabel!
    @IBOutlet private weak var likeLabel: UILabel!
    @IBOutlet private weak var dateLabel: UILabel!

    // MARK: - Lifecycle

    override func prepareForReuse() {
        super.prepareForReuse()
        postImage.image = nil
    }

    // MARK: - Private

    // MARK: - Public

    func setup(_ model: Post) {
        userName.text = model.userName
        likeLabel.text = String(model.likesCount ?? 0)
        commentLabel.text = String(model.commentsCount)
        dateLabel.text = model.dateCreated.toString(with: DateFormatter.PostDateFormat)
        postImage.loadImage(from: model.imageURL)
    }
}
