//
//  ViewController.swift
//  Vinditek
//
//  Created by Viktor Hodovanets on 25/03/2019.
//  Copyright © 2019 Viktor Hodovanets. All rights reserved.
//

import UIKit

class FeedViewController: UIViewController {

    // MARK: - Outlets

    @IBOutlet private weak var segmentedControl: UISegmentedControl!
    @IBOutlet private weak var tableView: UITableView!

    // MARK: - Properties
    private var postType: PostType {
        return PostType(rawValue: segmentedControl.selectedSegmentIndex) ?? .all
    }
    private let postService = PostService()

    // MARK: - Life cycle


    override func viewDidLoad() {
        super.viewDidLoad()

        setupTableView()
    }

    // MARK: - Private

    private func setupTableView() {
        tableView.register(PhotoPostCell.self)
        tableView.register(MessagePostCell.self)
    }

    private func messagePostCell(post: Post) -> UITableViewCell {
        guard let cell: MessagePostCell = tableView.cell()
            else {
                return UITableViewCell()
        }
        cell.setup(post)
        return cell
    }

    private func photoPostCell(post: Post) -> UITableViewCell {
        guard let cell: PhotoPostCell = tableView.cell()
            else {
                return UITableViewCell()
        }
        cell.setup(post)
        return cell
    }

    private func cellForPost(post: Post) -> UITableViewCell {
        guard let type = post.showType else { return UITableViewCell()}
        switch type {
        case .message:
            return messagePostCell(post: post)
        case .photo:
            return photoPostCell(post: post)
        case .all:
            return UITableViewCell()
        }
    }

    // MARK: - Public

    // MARK: - Actions

    @IBAction func segmentedChanged(_ sender: Any) {
        tableView.reloadData()
    }

    @IBAction func openSettings(_ sender: Any) {
        let vc = Storyboard.Main.instantiate(SettingsViewController.self)
        navigationController?.pushViewController(vc, animated: true)
    }
}

extension FeedViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return postService.posts(type: postType).count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let post = postService.post(type: postType, index: indexPath.row) else {
            return UITableViewCell()
        }
        return cellForPost(post: post)
    }
}

