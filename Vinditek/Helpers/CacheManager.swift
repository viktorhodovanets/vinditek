//
//  CacheManager.swift
//  Vinditek
//
//  Created by Viktor Hodovanets on 25/03/2019.
//  Copyright © 2019 Viktor Hodovanets. All rights reserved.
//

import UIKit

class CacheManager  {
    static let shared = CacheManager()
    var imageCache: NSCache<NSString, UIImage>

    private init() {
        self.imageCache = NSCache()
    }

    func getImage(for url: String) -> UIImage? {
        return self.imageCache.object(forKey: NSString(string: url))
    }

    func setImage(image: UIImage, key: String) -> Void {
        self.imageCache.setObject(image, forKey: NSString(string: key))
    }

    func changeCacheCountLimit(count: Int) {
        self.imageCache.countLimit = count
    }

    func changeCacheCostLimit(in mb: Int) {
        self.imageCache.totalCostLimit =  mb * 1024 * 1024
    }

    func clearCache() {
        imageCache.removeAllObjects()
    }
}
