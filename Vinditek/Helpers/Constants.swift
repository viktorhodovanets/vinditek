//
//  Constants.swift
//  Vinditek
//
//  Created by Viktor Hodovanets on 25/03/2019.
//  Copyright © 2019 Viktor Hodovanets. All rights reserved.
//

import Foundation

enum Constants {
    enum UserDefaults {
        static let cacheCountLimit = "cacheCountLimit"
        static let cacheCostLimit = "cacheCostLimit"
    }
    enum PostType {
        static let photo = "photoPost"
        static let message = "messagePost"
    }
}
