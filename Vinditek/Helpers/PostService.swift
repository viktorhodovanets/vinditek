//
//  PostService.swift
//  Vinditek
//
//  Created by Viktor Hodovanets on 25/03/2019.
//  Copyright © 2019 Viktor Hodovanets. All rights reserved.
//

import Foundation

class PostService  {

    // MARK: - Properties

    var messagePosts = [Post]()
    var photoPosts = [Post]()
    private var posts = [Post]() {
        didSet {
            messagePosts.removeAll()
            photoPosts.removeAll()
            messagePosts = posts.filter({ $0.showType == .message })
            photoPosts = posts.filter({ $0.showType == .photo })
        }
    }

    // MARK: - Init

    init() {
        setup()
    }

    // MARK: - Private

    private func setup() {
        guard let path = Bundle.main.path(forResource: "Data", ofType: "plist"),
            let xml = FileManager.default.contents(atPath: path) else {
                return
        }
        let decoder = PropertyListDecoder()
        posts = try! decoder.decode([Post].self, from: xml)
    }

    // MARK: - Public

    func posts(type: PostType) -> [Post] {
        switch type {
        case .all:
            return posts
        case .message:
            return messagePosts
        case .photo:
            return photoPosts
        }
    }

    func post(type: PostType, index: Int) -> Post? {
        guard let post = posts(type: type)[safe: index] else {
            return nil
        }
        return post
    }

}
