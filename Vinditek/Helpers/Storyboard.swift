//
//  Storyboard.swift
//  Vinditek
//
//  Created by Viktor Hodovanets on 25/03/2019.
//  Copyright © 2019 Viktor Hodovanets. All rights reserved.
//

import UIKit

enum Storyboard: String {
    case Main

    func instantiate<VC: UIViewController>(_ viewController: VC.Type) -> VC {
        guard let vc = UIStoryboard(name: self.rawValue, bundle: Bundle.main)
            .instantiateViewController(withIdentifier: "\(VC.self)") as? VC
            else {
                fatalError("Couldn't instantiate \(VC.self) from \(self.rawValue)")
        }
        return vc
    }
}
