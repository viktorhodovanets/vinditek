//
//  Date+Extension.swift
//  Vinditek
//
//  Created by Viktor Hodovanets on 25/03/2019.
//  Copyright © 2019 Viktor Hodovanets. All rights reserved.
//

import Foundation

extension Date {
    func toString(with fotmatter: DateFormatter) -> String {
        return fotmatter.string(from: self)
    }
}
