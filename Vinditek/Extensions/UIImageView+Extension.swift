//
//  UIImageView+Extension.swift
//  Vinditek
//
//  Created by Viktor Hodovanets on 25/03/2019.
//  Copyright © 2019 Viktor Hodovanets. All rights reserved.
//

import UIKit

fileprivate var matchingUrlKey: String = ""

extension UIImageView {
    private(set) var matchingUrl: String {
        get {
            guard let value = objc_getAssociatedObject(self, &matchingUrlKey)  else {
                return ""
            }
            return value as! String
        }
        set(newValue) {
            objc_setAssociatedObject(self, &matchingUrlKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }

    func setRounded() {
        self.layer.cornerRadius = (self.frame.width / 2)
        self.layer.masksToBounds = true
    }

    func loadImage(from link : String) {
        self.matchingUrl = link
        guard let url = URL(string: link) else { return }
        self.image = UIImage(named: "placeholder")

        DispatchQueue.global(qos: .background).async {
            if let cachedImage = CacheManager.shared.imageCache.object(forKey: NSString(string: link)) {
                DispatchQueue.main.async {
                    self.image = cachedImage
                    return
                }
            } else {
                self.loadFromNetwork(url: url)
            }
        }
    }
    
    private func loadFromNetwork(url: URL) {
        URLSession.shared.dataTask(with: url, completionHandler: { [weak self] (data, response, error) in
            guard let self = self else {return}
            if error != nil {
                print(error!)
                DispatchQueue.main.async {
                    self.image = UIImage(named: "placeholder")
                    return
                }
            }
            DispatchQueue.main.async {
                if data != nil {
                    if let image = UIImage(data: data!) {
                        CacheManager.shared.imageCache.setObject(image, forKey: NSString(string: url.absoluteString))
                        if self.matchingUrl == url.absoluteString {
                            self.image = image
                        }
                    }
                }
            }
        }).resume()
    }
}
