//
//  UITableView+Extension.swift
//  Vinditek
//
//  Created by Viktor Hodovanets on 25/03/2019.
//  Copyright © 2019 Viktor Hodovanets. All rights reserved.
//

import UIKit

extension UITableView {
    func register(_ type: UITableViewCell.Type) {
        let nib = UINib(nibName: "\(type)", bundle: nil)
        register(nib, forCellReuseIdentifier: "\(type)")
    }

    func cell<T: UITableViewCell>() -> T? {
        return dequeueReusableCell(withIdentifier: "\(T.self)") as? T
    }
}
