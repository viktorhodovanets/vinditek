//
//  Post.swift
//  Vinditek
//
//  Created by Viktor Hodovanets on 25/03/2019.
//  Copyright © 2019 Viktor Hodovanets. All rights reserved.
//

import Foundation

struct Post: Decodable {
    let identifier: Int
    let type: String
    let userName: String
    let imageURL: String
    let commentsCount: Int
    let dateCreated: Date

    let message: String?
    let likesCount: Int?

    var showType: PostType? {
        return PostType.fromString(type: type)
    }
}

enum PostType: Int {
    case all
    case message
    case photo

    static func fromString(type: String) -> PostType? {
        if type == Constants.PostType.photo {
            return .photo
        }
        if type == Constants.PostType.message {
            return .message
        }
        return nil
    }
}

