//
//  AppDelegate.swift
//  Vinditek
//
//  Created by Viktor Hodovanets on 25/03/2019.
//  Copyright © 2019 Viktor Hodovanets. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {


        return true
    }

    private func setupCache() {
        let cacheCountLimit = UserDefaults.standard.object(forKey: Constants.UserDefaults.cacheCountLimit) as? Int ?? 0
        let cacheCostLimit = UserDefaults.standard.object(forKey: Constants.UserDefaults.cacheCostLimit) as? Int ?? 0
        CacheManager.shared.changeCacheCountLimit(count: cacheCountLimit)
        CacheManager.shared.changeCacheCostLimit(in: cacheCostLimit)
    }
}
